# Dockerfile for PD-test-api

FROM debian:wheezy

MAINTAINER Kaur Jaanson <kjaanson@gmail.com>

ENV APP_HTTP_USER test
ENV APP_HTTP_PASSWD test
ENV MYSQL_USER testuser
ENV MYSQL_PASSWORD testpw
ENV MYSQL_DATABASE testdb

RUN apt-get update

# Install apache, PHP, and what else is needed
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install apache2 \
                                                      libapache2-mod-php5 \
                                                      php5-mysql \
                                                      php5-curl \
                                                      curl \
                                                      openssl \
                                                      ssl-cert \
                          													  mysql-server

###########################
# Configure Mysql
# Configure MySQL to accept outside container connections
RUN sed -i -e"s/^bind-address\s*=\s*127.0.0.1/bind-address = 0.0.0.0/" /etc/mysql/my.cnf

# Add database data
ADD db-sql/BX-Book-Ratings.sql	/opt/BX-Book-Ratings.sql
ADD db-sql/BX-Books.sql	/opt/BX-Books.sql
ADD db-sql/BX-Users.sql	/opt/BX-Users.sql

# Add database setup script
ADD config/db-setup.sh	/opt/db-setup.sh
RUN /bin/bash /opt/db-setup.sh


####################
# Configure Apache and PHP
# Set up the apache environment variables
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid

# Enable apache mods.
RUN a2enmod php5
RUN a2enmod rewrite
RUN a2enmod headers
RUN a2enmod ssl

# Disable default site
RUN a2dissite 000-default

# Update the PHP.ini file, enable <? ?> tags and quieten logging.
RUN sed -i "s/error_reporting = .*$/error_reporting = E_ALL/" /etc/php5/apache2/php.ini
RUN sed -i "s/display_errors = .*$/display_errors = On/" /etc/php5/apache2/php.ini

# Install php composer
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer

##################
# Set up API
# Copy site into place.
ADD api /var/www/api
ADD api-docs /var/www/api-docs

# Get dependencies
RUN cd /var/www/api && composer install

# Change file ownership permissions in api dir to apache user
RUN chown www-data:www-data -R /var/www/api

# Update the default apache site with the config we created.
ADD config/api.conf /etc/apache2/sites-enabled/api.conf

# Add startup script
ADD config/startup.sh /opt/startup.sh

# Expose container ports
EXPOSE 80
EXPOSE 443
EXPOSE 3306

# Run startup script
CMD ["/bin/bash","/opt/startup.sh"]
