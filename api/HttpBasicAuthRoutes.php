<?php

# Taken from Slim/Extras package, updated to work with routes
class HttpBasicAuthRoutes extends \Slim\Middleware
{
    /**
     * @var string
     */
    protected $realm;
    /**
     * @var string
     */
    protected $username;
    /**
     * @var string
     */
    protected $password;
    /**
     * @var string
     */
    protected $route;


    /**
     * Constructor
     *
     * @param   string  $username   The HTTP Authentication username
     * @param   string  $password   The HTTP Authentication password
     * @param   string  $realm      The HTTP Authentication realm
	 * @param	string  $route		The route
     */
    public function __construct($username, $password, $realm = 'Protected Area', $route = '')
    {
        $this->username = $username;
        $this->password = $password;
        $this->realm = $realm;
		    $this->route = $route;
    }
    /**
     * Call
     *
     * This method will check the HTTP request headers for previous authentication. If
     * the request has already authenticated, the next middleware is called. Otherwise,
     * a 401 Authentication Required response is returned to the client.
     */
    public function call()
    {
    // If route begins with specified route then it needs authentication
		if( 0 === strpos($this->app->request()->getPathInfo(), $this->route)) {
			$req = $this->app->request();
			$res = $this->app->response();
			$authUser = $req->headers('PHP_AUTH_USER');
			$authPass = $req->headers('PHP_AUTH_PW');
			if ($authUser && $authPass && $authUser === $this->username && $authPass === $this->password) {
				$this->next->call();
			} else {
				$res->status(401);
				$res->header('WWW-Authenticate', sprintf('Basic realm="%s"', $this->realm));
				echo json_encode(array(
					'message'=>'You are unauthorized to make this request.'
				));
			}
		} else {
			$this->next->call();
		}
    }
}
?>
