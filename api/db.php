<?php
function getDB() {
  $dbhost="localhost";
  $dbuser=getenv("MYSQL_USER");
  $dbpass=getenv("MYSQL_PASSWORD");
  $dbname=getenv("MYSQL_DATABASE");

  $connection = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser, $dbpass);
  $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  return $connection;
}


?>
