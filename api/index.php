<?php

require 'db.php';
require 'vendor/autoload.php';
require 'HttpBasicAuthRoutes.php';

$app = new \Slim\Slim();

// Accessing /books and /users is protected with Basic authentication
$basic_user=getenv("APP_HTTP_USER");
$basic_passw=getenv("APP_HTTP_PASSWD");
$app->add(new HttpBasicAuthRoutes($basic_user, $basic_passw, 'Protected','/books'));
$app->add(new HttpBasicAuthRoutes($basic_user, $basic_passw, 'Protected','/users'));

// Set all returned content to json
$app->response->headers->set('Content-Type', 'application/json');

// Replace HTML error page with json output message
$app->notFound(function () use ($app) {
	$app->response()->status(404);
    echo json_encode(array(
        'message'=>'Page not found'
        ));
});

/*
GET /books
  Summary: Returns list of all the book ISBN-s in the database
	200 - Success
	401 - Unauthorized
*/
$app->get('/books', function () use ($app) {
  $sql= "SELECT ISBN FROM `BX-Books`";

  try {
    $db = getDB();
    $statm = $db->query($sql);
    $tables = $statm->fetchAll(PDO::FETCH_COLUMN);
    $db=null;
    echo json_encode($tables);
  } catch(PDOExeption $e) {
    echo '{"message":' . json_encode($e->getMessage()) . '}';
  }
});

/*
GET /books/:book-isbn
  Summary: Retrives book info
	200 - Success
	401 - Unauthorized
	404 - Book not found
*/
$app->get('/books/:isbn', function ($isbn) use ($app) {
  $sql= "SELECT * FROM `BX-Books` WHERE ISBN=:isbn";

  try {
    $db = getDB();
    $statm = $db->prepare($sql);
    $statm->bindParam(':isbn',$isbn);
    $statm->execute();
    $tables = $statm->fetchAll(PDO::FETCH_OBJ);

    if(sizeof($tables)>0) {
      echo json_encode($tables[0]);
    } else {
      $app->response()->status(404);
      echo json_encode(array(
        'message'=>'Book ISBN '.$isbn.' does not exist.'
        ));
    }

    $db=null;

  } catch(PDOExeption $e) {
    echo '{"message":' . json_encode($e->getMessage()) . '}';
  }
});

/*
PUT /books/:isbn
  Summary: Adds new book or updates previously existing book
	200 - Book updated
  201 - Book added
  400 - Missing or invalid data
  401 - Unauthorized
*/
$app->put('/books/:isbn',function ($isbn) use ($app) {

  $jsonstr=$app->request()->getBody();
  $data=json_decode($jsonstr,true);

  if(!$data) {
    $app->response()->status(400);
    echo json_encode(array(
      'message'=>'Invalid JSON data'
      ));
    return;
  }

  //for filling missing values with empty data
  $temp_array=array("Book-Title"=>'',
                    "Book-Author"=>'',
                    "Year-Of-Publication"=>0,
                    "Publisher"=>'',
                    "Image-URL-S"=>'',
                    "Image-URL-M"=>'',
                    "Image-URL-L"=>''
                   );
  $data=array_replace($temp_array,$data);

  //ISBN validation
  if(strlen($isbn)>13) {
    $app->response()->status(400);
    echo json_encode(array(
      'message'=>'Book ISBN out of bounds (max 13 chars)'
      ));
    return;
  }

  try {
    $db = getDB();
    $statm = $db->prepare("SELECT * FROM `BX-Books` WHERE `ISBN` = :isbn");
    $statm->bindParam(':isbn',$isbn);
    $statm->execute();
    $count = $statm->rowCount();

    if($count>0) { //UPDATE
      $statm = $db->prepare("UPDATE `BX-Books` SET `Book-Title`=:title,
                                                   `Book-Author`=:author,
                                                   `Year-Of-Publication`=:yop,
                                                   `Publisher`=:publisher,
                                                   `Image-URL-S`=:images,
                                                   `Image-URL-M`=:imagem,
                                                   `Image-URL-L`=:imagel
                                               WHERE ISBN=:isbn");
      $statm->bindParam(':isbn',$isbn);
      $statm->bindParam(':title',$data["Book-Title"]);
      $statm->bindParam(':author',$data["Book-Author"]);
      $statm->bindParam(':yop',$data["Year-Of-Publication"]);
      $statm->bindParam(':publisher',$data["Publisher"]);
      $statm->bindParam(':images',$data["Image-URL-S"]);
      $statm->bindParam(':imagem',$data["Image-URL-M"]);
      $statm->bindParam(':imagel',$data["Image-URL-L"]);
      $statm->execute();
      $app->response()->status(200);
      echo json_encode(array(
       'message'=>'Book ISBN '.$isbn.' updated'
      ));

    } else { //INSERT
      $statm = $db->prepare("INSERT INTO `BX-Books` VALUES (:isbn,:title,:author,:yop,:publisher,:images,:imagem,:imagel)");
      $statm->bindParam(':isbn',$isbn);
      $statm->bindParam(':title',$data["Book-Title"]);
      $statm->bindParam(':author',$data["Book-Author"]);
      $statm->bindParam(':yop',$data["Year-Of-Publication"]);
      $statm->bindParam(':publisher',$data["Publisher"]);
      $statm->bindParam(':images',$data["Image-URL-S"]);
      $statm->bindParam(':imagem',$data["Image-URL-M"]);
      $statm->bindParam(':imagel',$data["Image-URL-L"]);
      $statm->execute();
      $app->response()->status(201);
      echo json_encode(array(
        'message'=>'Book ISBN '.$isbn.' added'
        ));
    }
    $db=null;
  } catch(PDOExeption $e) {
    echo '{"message":' . json_encode($e->getMessage()) . '}';
  }

});

/*
DELETE /books/:isbn
  Summary: Deletes book
	200 - Success
	204 - No Content (non existent isbn, maybe already deleted)
  401 - Unauthorized
*/
$app->delete('/books/:isbn',function($isbn) use ($app) {
  $sql= "DELETE FROM `BX-Books` WHERE ISBN=:isbn";

  try {
    $db = getDB();
    $statm = $db->prepare($sql);
    $statm->bindParam(':isbn',$isbn);
    $statm->execute();
    $count = $statm->rowCount();
    $db=null;

    //If there was deleted rows return 200, if there was not then return 204 (No content)
    if($count>0) {
      $app->response()->status(200);
      echo json_encode(array(
        'message'=>'Book ISBN '.$isbn.' deleted'
        ));
    } else {
      $app->response()->status(204);
	    echo json_encode(array(
        'message'=>'Nothing to delete'
        ));
    }


  } catch(PDOExeption $e) {
    echo '{"message":' . json_encode($e->getMessage()) . '}';
  }
});

/*
GET /users
  Summary: Returns list of all user id-s in database
	200 - Success, return list of users ids
  401 - Unauthorized
*/
$app->get('/users', function() use ($app) {
  $sql= "SELECT `User-ID` FROM `BX-Users`";

  try {
    $db = getDB();
    $statm = $db->query($sql);
    $tables = $statm->fetchAll(PDO::FETCH_COLUMN);
    $db=null;
    echo json_encode($tables);
  } catch(PDOExeption $e) {
    echo '{"message":' . json_encode($e->getMessage()) . '}';
  }
});

/*
POST /users
  Summary: Adds new user with incremental key. Returns key in response.
	201 - Success
  400 - Missing or invalid data
  401 - Unauthorized
*/
$app->post('/users', function() use ($app) {
  $jsonstr=$app->request()->getBody();
  $data=json_decode($jsonstr,true);

  //Check if JSON data was decoded
  if(!$data) {
    $app->response()->status(400);
    echo json_encode(array(
      'message'=>'Invalid JSON data'
      ));
    return;
  }
  //Check if location and age exists in data
  if(!isset($data["Location"])) {
    $app->response()->status(400);
    echo json_encode(array(
      'message'=>'No user location data'
      ));
    return;
  }
	//if no Age, set it to null
  if(!isset($data["Age"])) {
		$data["Age"]=null;
  }

  try {
    $db = getDB();
    $query = $db->query("SELECT MAX(`User-ID`)+1 FROM `BX-Users`");
    $max_user_id = $query->fetchColumn();

    $statm = $db->prepare("INSERT INTO `BX-Users` VALUES (:userid,:location,:age)");
    $statm->bindParam(":userid",$max_user_id);
    $statm->bindParam(":location",$data["Location"]);
    $statm->bindParam(":age",$data["Age"]);
    $statm->execute();

    $app->response()->status(201);
    echo json_encode(array(
      'message'=>'User added',
      'User-ID'=>$max_user_id
      ));

    $db = null;
  } catch(PDOExeption $e) {
    echo '{"message":' . json_encode($e->getMessage()) . '}';
  }
});

/*
GET /users/:user_id
  Summary: Return user data
	200 - Success
  401 - Unauthorized
  404 - User not found
*/
$app->get('/users/:userid', function($userid) use ($app) {
  $sql= "SELECT * FROM `BX-Users` WHERE `User-ID`=:userid";

  try {
    $db = getDB();
    $statm = $db->prepare($sql);
    $statm->bindParam(':userid',$userid);
    $statm->execute();
    $tables = $statm->fetchAll(PDO::FETCH_OBJ);
    $db=null;

    if(sizeof($tables)>0) {
      echo json_encode($tables[0]);
    } else {
      $app->response()->status(404);
      echo json_encode(array(
        'message'=>'User '.$userid.' does not exist'
        ));
    }

  } catch(PDOExeption $e) {
    echo '{"message":' . json_encode($e->getMessage()) . '}';
  }
});

/*
PUT /users/:user_id
  Summary: Adds or updates user
	200 - User updated
  201 - User added
  400 - Missing or invalid data
  401 - Unauthorized
*/
$app->put('/users/:userid', function($userid) use ($app) {
  $jsonstr=$app->request()->getBody();
  $data=json_decode($jsonstr,true);

  if(!$data) {
    $app->response()->status(400);
    echo json_encode(array(
      'message'=>'Invalid JSON data'
      ));
    return;
  }

  //Check if location and age exists in data
  if(!isset($data["Location"])) {
    $app->response()->status(400);
    echo json_encode(array(
      'message'=>'No user location data'
      ));
    return;
  }
  if(!isset($data["Age"])) {
    $data["Age"]=null;
  }

  try {
    $db = getDB();

    //See if user exists or not
    $statm = $db->prepare("SELECT * FROM `BX-Users` WHERE `User-ID` = :userid");
    $statm->bindParam(':userid',$userid);
    $statm->execute();
    $count = $statm->rowCount();

    if($count>0) { //UPDATE
      $statm = $db->prepare("UPDATE `BX-Users` SET `Location`=:location,`Age`=:age WHERE `User-ID`=:userid");
      $statm->bindParam(':userid',$userid);
      $statm->bindParam(':location',$data['Location']);
      $statm->bindParam(':age',$data['Age']);
      $statm->execute();

      $app->response()->status(200);
      echo json_encode(array(
        'message'=>'User updated'
        ));

    } else { //INSERT
      $db = getDB();
      $statm = $db->prepare("INSERT INTO `BX-Users` VALUES (:userid,:location,:age)");
      $statm->bindParam(":userid",$userid);
      $statm->bindParam(":location",$data["Location"]);
      $statm->bindParam(":age",$data["Age"]);
      $statm->execute();

      $app->response()->status(201);
      echo json_encode(array(
        'message'=>'User added'
        ));
    }


    $db = null;
  } catch(PDOExeption $e) {
    echo '{"message":' . json_encode($e->getMessage()) . '}';
  }
});


/*
DELETE /users/:user_id
  Summary: Deletes user
	200 - Successful delete
	204 - Nothing to delete
  401 - Unauthorized
*/
$app->delete('/users/:userid', function($userid) use ($app) {
  $sql= "DELETE FROM `BX-Users` WHERE `User-ID`=:userid";

  try {
    $db = getDB();
    $statm = $db->prepare($sql);
    $statm->bindParam(':userid',$userid);
    $statm->execute();
    $count = $statm->rowCount();
    $db=null;

    //If there were deleted rows return 200, if there were none then return 204 (No content)
    if($count>0) {
      $app->response()->status(200);
      echo json_encode(array(
        'message'=>'User '.$userid.' deleted'
        ));
    } else {
      $app->response()->status(204);
      echo json_encode(array(
        'message'=>'No such user'
        ));
    }

  } catch(PDOExeption $e) {
    echo '{"message":' . json_encode($e->getMessage()) . '}';
  }
});

/*
GET /users/:userid/ratings
  Summary: Returns list of all the book ISBN-s in the database
	200 - Success
	401 - Unauthorized
	404 - No ratings
*/
$app->get('/users/:userid/ratings', function($userid) use ($app) {
	$sql= "SELECT * FROM `BX-Book-Ratings` WHERE `BX-Book-Ratings`.`User-ID` = :userid";

  try {
    $db = getDB();
    $statm = $db->prepare($sql);
    $statm->bindParam(':userid',$userid);
    $statm->execute();
    $tables = $statm->fetchAll(PDO::FETCH_OBJ);
    $count = $statm->rowCount();
    $db=null;

    if($count>0) {
      $app->response()->status(200);
      echo json_encode($tables);
    } else {
      $app->response()->status(404);
      echo json_encode(array(
        'message'=>'No ratings'
        ));
    }


  } catch(PDOExeption $e) {
    echo '{"message":' . json_encode($e->getMessage()) . '}';
  }

});

/*
GET /public/books/:isbn/country/:country
  Summary: Returns list of country specific book ratings
	200 - Success
	401 - Unauthorized
	404 - No ratings
*/
$app->get('/public/books/:isbn/country/:country', function($isbn,$country) use ($app) {
  $sql= "SELECT
						ratings.`User-ID`,
						ratings.`Book-Rating`,
						users.`Location`
				 FROM
				 		`BX-Book-Ratings` ratings,
						`BX-Users` users
				 WHERE ratings.`User-ID` = users.`User-ID` AND ratings.`ISBN` = :isbn AND TRIM(SUBSTRING_INDEX(users.`Location`,',',-1)) = :country";

  try {
    $db = getDB();
    $statm = $db->prepare($sql);
    $statm->bindParam(':isbn',$isbn);
    $statm->bindParam(':country',$country);
    $statm->execute();
    $tables = $statm->fetchAll(PDO::FETCH_OBJ);
    $count = $statm->rowCount();
    $db=null;

    if($count>0) {
      $app->response()->status(200);
      echo json_encode($tables);
    } else {
      $app->response()->status(404);
      echo json_encode(array(
        'message'=>'No ratings'
        ));
    }


  } catch(PDOExeption $e) {
    echo '{"message":' . json_encode($e->getMessage()) . '}';
  }
});

/*
GET /books/:isbn/ratings
  Summary: Returns list of all the book ISBN-s in the database
	200 - Success
	401 - Unauthorized
	404 - No ratings
*/
$app->get('/books/:isbn/ratings', function($isbn) use ($app) {
	$sql= "SELECT * FROM `BX-Book-Ratings` WHERE `BX-Book-Ratings`.`ISBN` = :isbn";

  try {
    $db = getDB();
    $statm = $db->prepare($sql);
    $statm->bindParam(':isbn',$isbn);
    $statm->execute();
    $tables = $statm->fetchAll(PDO::FETCH_OBJ);
    $count = $statm->rowCount();
    $db=null;

    if($count>0) {
      $app->response()->status(200);
      echo json_encode($tables);
    } else {
      $app->response()->status(404);
      echo json_encode(array(
        'message'=>'No ratings'
        ));
    }


  } catch(PDOExeption $e) {
    echo '{"message":' . json_encode($e->getMessage()) . '}';
  }

});

/*
GET /books/:isbn/ratings/:userid
  Summary: Returns list of all the book ISBN-s in the database for specific user
	200 - Success
	401 - Unauthorized
	404 - No ratings
*/
$app->get('/books/:isbn/ratings/:userid', function($isbn,$userid) use ($app) {
	$sql= "SELECT * FROM `BX-Book-Ratings` WHERE `BX-Book-Ratings`.`ISBN` = :isbn AND `BX-Book-Ratings`.`User-ID` = :userid";

  try {
    $db = getDB();
    $statm = $db->prepare($sql);
    $statm->bindParam(':isbn',$isbn);
		$statm->bindParam(':userid',$userid);
    $statm->execute();
    $tables = $statm->fetchAll(PDO::FETCH_OBJ);
    $count = $statm->rowCount();
    $db=null;

    if($count>0) {
      $app->response()->status(200);
      echo json_encode($tables[0]);
    } else {
      $app->response()->status(404);
      echo json_encode(array(
        'message'=>'No ratings found'
        ));
    }

  } catch(PDOExeption $e) {
    echo '{"message":' . json_encode($e->getMessage()) . '}';
  }
});

/*
PUT /books/:isbn/ratings/:userid
	Summary: Adds or updates user book rating
	200 - Rating updated
	201 - Rating added
	400 - Missing or invalid data
	401 - Unauthorized
	422 - User or book is not in database
*/
$app->put('/books/:isbn/ratings/:userid', function($isbn,$userid) use ($app) {
	$jsonstr=$app->request()->getBody();
	$data=json_decode($jsonstr,true);

	if(!$data) {
		$app->response()->status(400);
		echo json_encode(array(
			'message'=>'Invalid JSON data'
			));
		return;
	}

	if(!isset($data["Book-Rating"])) {
		$app->response()->status(400);
		echo json_encode(array(
			'message'=>'No rating data'
			));
		return;
	}

	try {
    $db = getDB();


		//Check if book exists in DB
		$statm = $db->prepare("SELECT * FROM `BX-Books` WHERE `ISBN` = :isbn");
		$statm->bindParam(':isbn',$isbn);
		$statm->execute();
		$count=$statm->rowCount();
		if($count<1) {
			$app->response()->status(422);
			echo json_encode(array(
				'message'=>'Book does not exist in database'
				));
			$db=null;
			return;
		}

		//Check if user exists in DB
		$statm = $db->prepare("SELECT * FROM `BX-Users` WHERE `User-ID` = :userid");
		$statm->bindParam(':userid',$userid);
		$statm->execute();
		$count=$statm->rowCount();
		if($count<1) {
			$app->response()->status(422);
      echo json_encode(array(
        'message'=>'User does not exist in database'
        ));
			$db=null;
			return;
		}

    //See if rating exists or not
    $statm = $db->prepare("SELECT * FROM `BX-Book-Ratings` WHERE `User-ID` = :userid AND `ISBN` = :isbn");
    $statm->bindParam(':userid',$userid);
		$statm->bindParam(':isbn',$isbn);
    $statm->execute();
    $count = $statm->rowCount();

    if($count>0) { //UPDATE
      $statm = $db->prepare("UPDATE `BX-Book-Ratings` SET `Book-Rating`=:rating WHERE `User-ID`=:userid AND `ISBN`=:isbn");
			$statm->bindParam(':isbn',$isbn);
      $statm->bindParam(':userid',$userid);
      $statm->bindParam(':rating',$data['Book-Rating']);
      $statm->execute();

      $app->response()->status(200);
      echo json_encode(array(
        'message'=>'Rating updated'
        ));

    } else { //INSERT
      $db = getDB();
      $statm = $db->prepare("INSERT INTO `BX-Book-Ratings` VALUES (:userid,:isbn,:rating)");
			$statm->bindParam(":isbn",$isbn);
      $statm->bindParam(":userid",$userid);
      $statm->bindParam(":rating",$data["Book-Rating"]);
      $statm->execute();

      $app->response()->status(201);
      echo json_encode(array(
        'message'=>'Rating added'
        ));
    }


    $db = null;
  } catch(PDOExeption $e) {
    echo '{"message":' . json_encode($e->getMessage()) . '}';
  }

});


/*
DELETE /books/:isbn/ratings/:userid
  Summary: Deletes book rating made by user from the database
	200 - Success
	204 - Nothing to delete
	401 - Unauthorized
*/
$app->delete('/books/:isbn/ratings/:userid', function($isbn,$userid) use ($app) {
	$sql= "DELETE FROM `BX-Book-Ratings` WHERE `BX-Book-Ratings`.ISBN=:isbn AND `BX-Book-Ratings`.`User-ID` = :userid";

  try {
    $db = getDB();
    $statm = $db->prepare($sql);
    $statm->bindParam(':isbn',$isbn);
		$statm->bindParam(':userid',$userid);
    $statm->execute();
    $count = $statm->rowCount();
    $db=null;

    //If there was deleted rows return 200, if there was not then return 204 (No content)
    if($count>0) {
      $app->response()->status(200);
      echo json_encode(array(
        'message'=>'Book rating deleted'
        ));
    } else {
      $app->response()->status(204);
	    echo json_encode(array(
        'message'=>'Nothing to delete'
        ));
    }


  } catch(PDOExeption $e) {
    echo '{"message":' . json_encode($e->getMessage()) . '}';
  }
});


$app->run();

?>
