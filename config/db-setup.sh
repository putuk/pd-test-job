#!/bin/bash

#Set up mysql user and database

mysql_install_db

/usr/bin/mysqld_safe &
sleep 10s
echo "Creating API database, user and setting permissions"
echo "CREATE DATABASE ${MYSQL_DATABASE};GRANT ALL ON ${MYSQL_DATABASE}.* TO ${MYSQL_USER}@'%' IDENTIFIED BY '${MYSQL_PASSWORD}' WITH GRANT OPTION; FLUSH PRIVILEGES" | mysql

echo "Loading BX-Book-Ratings.sql"
mysql testdb < /opt/BX-Book-Ratings.sql
echo "Loading BX-Books.sql"
mysql testdb < /opt/BX-Books.sql
echo "Loading BX-Users.sql"
mysql testdb < /opt/BX-Users.sql

killall mysqld
sleep 10s
