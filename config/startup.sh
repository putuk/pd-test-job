#!/bin/bash

# If no mounted database exist then create database again (happens when outside volume is mounted to /var/lib/mysql for data persistance)
if [ ! -f /var/lib/mysql/ibdata1 ]; then
    /bin/bash /opt/db-setup.sh
fi

# run mysql and apache
/usr/bin/mysqld_safe &

/usr/sbin/apache2ctl -D FOREGROUND
