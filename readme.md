Summary
-------
Test work for PD. RESTful API for accessing book ratings database. Uses Basic authentication for access control. Built on PHP Slim micro framework (http://www.slimframework.com/). Documentation generated with Swagger-UI. Deployable using Docker container. Accepts both HTTP and HTTPS requests.

Requirements
------------
  * Docker (https://www.docker.com/)

Configuration
------------
No configuration nessesary. If required, Basic authentication user credentials can be changed using environment variables APP_HTTP_USER and APP_HTTP_PASSWD. Default values for both are `test`.

Installation & Deployment
------------
*Build docker image*
```
docker build -t pd-test https://bitbucket.org/putuk/pd-test-job.git
```

*Start API container with default settings (Basic auth user credentials test/test)*
```
docker run -d -p 80:80 -p 443:443 pd-test
```

*Start API container with kala/maja as user credentials*
```
docker run -d -p 80:80 -p 443:443 -e "APP_HTTP_USER=kala" -e "APP_HTTP_PASSWD=maja" pd-test
```

API base will then be `http://localhost/api` or `https://localhost/api`. API docs can be accessed at `http://localhost/api-docs` or `https://localhost/api-docs`. Sample requests to API can be made using Swagger UI (for default user/password use 'test'/'test'). If using boot2docker in Windows or Mac, replace localhost with vm ip, which can be obtained using `boot2docker ip` command.